package strategy;

import java.util.Random;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Anna generoitavan aineiston lukumäärä");
		int MAX = Integer.parseInt(sc.nextLine());
		
		
		int[] taul = new int[MAX];
		Random r = new Random();
		for (int i = 0; i < MAX; i++) {
			taul[i] = r.nextInt(100000);
			System.out.print(taul[i] + " ");
			if (i > 0 && i % 40 == 0)
				System.out.println();
		}
		
		StrategyInterface strategy = new Strategy();
		strategy.Sort(taul, MAX);
	}

}

