package strategy;

public class Strategy implements StrategyInterface {
	
	public Strategy() {
	}


	@Override
	public void Sort(int[] taulukko, int MAX) {
		if(MAX <= 10000) {
			StrategyInterface merge = new MergeSort();
			merge.Sort(taulukko, MAX);
		} else if (MAX<=50000) {
			StrategyInterface select = new SelectSort();
			select.Sort(taulukko, MAX);
		} else {
			StrategyInterface quick = new QuickSort();
			quick.Sort(taulukko, MAX);
		}
		
	}
}
