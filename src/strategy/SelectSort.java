package strategy;

import java.util.Random;

public class SelectSort implements StrategyInterface {
	int i, j, k, apu, pienin;

	@Override
	public void Sort(int[] taulukko, int MAX) {
		selectSort(taulukko, MAX);

	}

	public void selectSort(int[] taul, int MAX) {

		System.out.println("\nSuoritetaan valintalajittelu, paina Enter ");
		Reader.read();
		long start = System.currentTimeMillis();
		for (i = 0; i < MAX; i++) {
			pienin = i;

			for (j = i + 1; j < MAX; j++) {
				if (taul[j] < taul[pienin]) {
					pienin = j;
				}
			}
			if (pienin != i) {
				apu = taul[pienin];
				taul[pienin] = taul[i];
				taul[i] = apu;
			}
		}
		for (i = 0; i < MAX; i++) {
			System.out.print(taul[i] + " ");
			if (i > 0 && i % 40 == 0)
				System.out.println();
		}
		long end = System.currentTimeMillis();
		long timer = end -= start;
		System.out.println("\nKuittaa tulos, paina Enter");
		System.out.println("Lajitteluun käytetty aika = " + timer);
		Reader.read();

	}

}
